# Ansible Role: Prometheus

Prometheus is an opensource monitoring solution that gathers time series based numerical data. It is a project which was started by Google’s ex-employees at SoundCloud.

To monitor your services and infra with prometheus your service need to expose an endpoint in the form of port or url. For ex:- {{ localhost:9090 }}. The endpoint is HTTP interface that exposes the metrics.

# Requirements

There is no particular requirment for running this role. As this role is platform independent for centos 6 or 7 and ubuntu 14 or 16. The only dependency for centos 6 is libselinux python and we have included that as well.
The basic requirments are:-
- Centos/Ubuntu Server
- Python should be installed on the target server so that ansible can perform task
- libselinux-python should be available on os
You can define any prometheus version that you want to install on your server.

|Variable | Description|
|---------|------------|
|prometheus_version | Prometheus will be downloaded from github releases, so you have to define version in [defaults]|
|prometheus_ip | IP of the server on which prometheus will expose its UI |
|prometheus_port | Port no. of server on which prometheus should listen |
|base_download_url | Base url of prometheus release |



Directory structure:-
```bash
prometheus
├── defaults
│   └── main.yml
├── handlers
│   └── main.yml
├── hosts
├── README.md
├── site.yml
├── tasks
│   ├── debian.yml
│   ├── main.yml
│   └── redhat.yml
└── templates
    ├── prometheus.init.j2
    ├── prometheus.service.j2
    └── prometheus.yml.j2
