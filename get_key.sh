#!/bin/bash
cd /var/lib/jenkins/
echo -e "\n\n\n" | ssh-keygen -t rsa -q -P ""
cd .ssh
cat > config <<EOL
Host *
IdentityFile /var/lib/jenkins/.ssh/keys/amit.pem
EOL
mkdir keys
cd keys
aws s3 cp s3://myterra642/amit.pem ./
chmod 400 amit.pem
